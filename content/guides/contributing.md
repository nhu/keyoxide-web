# Contributing to Keyoxide

Keyoxide is more than this website. It's a project that aims to make cryptography more accessible to everyone. Keyoxide is part of a larger community of people working hard to develop tools that add privacy and security to our digital online lives. Remember: privacy is not a luxury.

## As a developer

As Keyoxide is an open-source project licensed under the permissive [MIT License](https://codeberg.org/keyoxide/web/src/branch/main/LICENSE), everyone is welcome and encouraged to contribute. This can be done in various forms:

*   [Open an issue](https://codeberg.org/keyoxide/web/issues) to request changes, new features or simply get help.
*   [Open a PR](https://codeberg.org/keyoxide/web/pulls) to directly integrate your own changes and new features.

## Not a developer?

Not a developer? Not a problem? You could:

*   Learn more about the importance of online privacy and security and advocate for it (much needed!)
*   Write guides for others and help each other out.
*   Start using decentralized OpenPGP identity keys.
*   Spread the word about Keyoxide and OpenPGP keys in general.
*   Talk to persons you know using siloed or closed-source alternatives to Keyoxide.
